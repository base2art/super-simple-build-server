<%@ Page Language="C#" MasterPageFile="~/standard.master" Title="Home" %>
<%@ Import Namespace="System.IO" %>  

<script runat="server">

private string path = HttpContext.Current.Server.MapPath("~/app_data");

protected override void OnLoad(EventArgs e)
{

    base.OnLoad(e);
    myRepeater.DataSource = Directory.EnumerateFiles(path, "*.git-proj").Select(f => new { Filename=Path.GetFileNameWithoutExtension(f) });
    myRepeater.DataBind();
}
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="MainPanel" Runat="Server">
    
    
    <asp:repeater runat="server" id="myRepeater">
    <HeaderTemplate>  
    <table style="min-width: 400px;">  
    <tr>
      <th style="text-align: left;">
        Project Name
      </th>
      <th style="text-align: left;">
      Action
      </th>
    </tr>  
    </HeaderTemplate>
    <ItemTemplate> 
    <tr>  
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    <tr>
      <td>
        <asp:Label runat="server" Text='<%#Eval("Filename") %>'/>  
      </td>
      <td>
        <form action="/build.aspx" method="POST">
          <input type="hidden" name="buildType" value="<%#Eval("Filename") %>"/>
          <input type="submit" value="Build">
        </form>
      </td>
    </tr>  
    </ItemTemplate>  
    <FooterTemplate>  
    </table>  
    </FooterTemplate>
    </asp:repeater>
    
    <br />
    <br />



</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="LeftPanel" Runat="Server" >
    
</asp:content>