<%@ Page Language="C#" MasterPageFile="~/standard.master" Title="Home" %>
<%@ Import Namespace="System.IO" %>  

<script runat="server">

private string path = HttpContext.Current.Server.MapPath("~/app_data");

protected override void OnLoad(EventArgs e)
{

    base.OnLoad(e);
}
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="MainPanel" Runat="Server">
    
    

    

    <h2>How it works?</h2>
    <p>Triggering a build will run a git checkout (git must be in the system environment PATH)</p>
    <p>A file will be written to the checkout Directory called `.branch-name` and `.version` containing the corresponding information</p>
    <p>Each Git Project must contain a powershell6 compatible build script, called bob.ps1, which the build server executes</p>
    <p>When the build finishes it will look for a file in the checkout Directory called `.artifacts` which should be a file checked in to source control or generated as part of the build. This file should be a <a href="https://git-scm.com/docs/gitignore">git ignore compatible file</a>.</p>
    <p>All files in the checkout directory matching the patterns in the `.artifacts` files will be pushed to the url in the `~/app_data/global/push.url` file.</p>
    

    <br />
    <b>
        To use this app deploy your ssh keys to: /c/WINDOWS/system32/config/systemprofile/.ssh'
        make sure that the IIS_IUSRS has full control over the files in this folder.
    </b>

</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="LeftPanel" Runat="Server" >
    
</asp:content>