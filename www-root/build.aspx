<%@ Page Language="C#" MasterPageFile="~/standard.master" Title="Home" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>


<script runat="server">

private string path = HttpContext.Current.Server.MapPath("~/app_data");
private string globalPath = HttpContext.Current.Server.MapPath("~/app_data/global");
private bool isDebug = false;
private Dictionary<string, string> globalConfig = new Dictionary<string, string>();


protected Process GetProcess(StreamWriter outputStream)
{
    var process = new Process();

    process.StartInfo.LoadUserProfile = true;
    process.StartInfo.UseShellExecute = false;
    process.StartInfo.RedirectStandardOutput = true;
    process.StartInfo.RedirectStandardError = true;
    process.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                outputStream.WriteLine(e.Data);
                outputStream.Flush();
            }
        });

    process.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                outputStream.WriteLine(e.Data);
                outputStream.Flush();
            }
        });
    return process;
}

protected int CompleteProcess(Process process)
{
    process.Start();
    process.BeginOutputReadLine();
    process.BeginErrorReadLine();
    process.WaitForExit(); 
    return process.ExitCode;
}



protected string Checkout(string appName, string version, string workingDir, string procFile, string url, string branch)
{
    using(var fs = File.Open(procFile, FileMode.Append, FileAccess.Write, FileShare.Read))
    {
        using (var outputStream = new StreamWriter(fs))
        {
            using (Process process = this.GetProcess(outputStream))
            {
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = "git";
                if (String.IsNullOrWhiteSpace(branch))
                {
                    process.StartInfo.Arguments = "clone \"" + url + "\" " + version;
                }
                else 
                {
                    process.StartInfo.Arguments = "clone -b \"" + branch + "\" \"" + url + "\" " + version;
                }

                var result = CompleteProcess(process);
                if (result != 0)
                {
                    throw new Exception("Build Failed");
                }

                return Path.Combine(workingDir, version);
            }
        }
    }
}

protected void Build(string appName, string version, string workingDir, string procFile)
{
    using(var fs = File.Open(procFile, FileMode.Append, FileAccess.Write, FileShare.Read))
    {
        using (var outputStream = new StreamWriter(fs))
        {
            using (Process process = this.GetProcess(outputStream))
            {
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = "pwsh";
                process.StartInfo.Arguments = "-File bob.ps1 default";
                var result = CompleteProcess(process);
                if (result != 0)
                {
                    throw new Exception("Build Failed");
                }
            }
        }
    }
}

private void AggFiles(DirectoryInfo curr, string relPath, List<string> agg)
{
    foreach (var dir in curr.GetDirectories())
    {
        AggFiles(dir, Path.Combine(relPath, dir.Name), agg);
    }

    foreach (var file in curr.GetFiles())
    {
        agg.Add(Path.Combine(relPath, file.Name));
    }
}

protected void Publish(string appName, string version, string workingDir, string procFile)
{
    using(var fs = File.Open(procFile, FileMode.Append, FileAccess.Write, FileShare.Read))
    {
        using (var outputStream = new StreamWriter(fs))
        {
            var artifacts = File.ReadAllLines(Path.Combine(workingDir, ".artifacts")).Where(x=> !string.IsNullOrWhiteSpace(x));
            
            string logDirs = Path.Combine(@"c:\cicd\simple-server\logs", appName, version);
            String logFile = Path.Combine(logDirs, version + ".log");
            var root = new DirectoryInfo(workingDir);
            var files = new List<string>();
            var filteredFiles = new HashSet<string>();
            AggFiles(root, "", files);
            
            outputStream.WriteLine("Artifact Lookups...");
            foreach(var artifact in artifacts) 
            {
                outputStream.WriteLine(artifact);
                var regex = "^" + Regex.Escape(artifact).Replace("\\*", ".*").Replace("\\?", ".") + "$";
                Regex rgx = new Regex(regex, RegexOptions.IgnoreCase);
                foreach(var file in files)
                {
                    if (rgx.IsMatch(file) || rgx.IsMatch(file.Replace("\\", "/")))
                    {
                        filteredFiles.Add(file);
                    }
                }
            }
            
            
            outputStream.WriteLine("Artifacts to publish:");

            var wc = new System.Net.WebClient();


            foreach(var artifact in filteredFiles)
            {
                outputStream.WriteLine(artifact);
                var fileName = artifact;
                if (this.isDebug) 
                {
                    fileName = ".deploy/test-deployment." + version + ".zip";
                    File.Move(Path.Combine(workingDir, artifact), Path.Combine(workingDir, fileName));
                }
                
                outputStream.WriteLine("Pushing: " + fileName);
                wc.UploadFile(globalConfig["push.url"], Path.Combine(workingDir, fileName));
            }
        }
    }
}

protected  void BuildProject(string appName, string version, Dictionary<string, string> config)
{
    string checkoutDirs = Path.Combine(@"c:\cicd\simple-server\checkouts", appName);
    string logDirs = Path.Combine(@"c:\cicd\simple-server\logs", appName, version);
    Directory.CreateDirectory(checkoutDirs);
    Directory.CreateDirectory(logDirs);


    String procFileC = Path.Combine(logDirs, version + ".checkout-proc");
    File.WriteAllText(procFileC, "");
    String procFileB = Path.Combine(logDirs, version + ".build-proc");
    File.WriteAllText(procFileB, "");
    String procFileP = Path.Combine(logDirs, version + ".publish-proc");
    File.WriteAllText(procFileP, "");

    String logFile = Path.Combine(logDirs, version + ".log");
    File.WriteAllText(logFile, "");

    String state = Path.Combine(logDirs, version + ".state");
    File.WriteAllText(state, "Queued");

    File.AppendAllText(logFile, "Starting... \r\n");

    // created only for debugging purposes
    string workingDir = Path.Combine(checkoutDirs, version);

    try
    {
        File.AppendAllText(logFile, "Running in " + workingDir + "\r\n");
        File.WriteAllText(state, "Claimed");

        string gitUrl = config["git-proj"];
        string branch = config.ContainsKey("branch") ? config["branch"] : "master";

        Thread t = new Thread(() => {
            try
            {
                try
                {
                    // checkout Dirs is right git will create the dir
                    File.WriteAllText(state, "InProgress-Checkout");
                    workingDir = this.Checkout(appName, version, checkoutDirs, procFileC, gitUrl, branch);
                }
                catch (Exception ex)
                {
                    File.WriteAllText(state, "Complete-CheckoutFailed");
                    throw;
                }

                File.WriteAllText(Path.Combine(workingDir, ".version"), version);
                File.WriteAllText(Path.Combine(workingDir, ".branch-name"), branch);

                try
                {
                    File.WriteAllText(state, "InProgress-Build");
                    this.Build(appName, version, workingDir, procFileB);
                }
                catch (Exception ex)
                {
                    File.WriteAllText(state, "Complete-BuildFailed");
                    throw;
                }
                try
                {
                    File.WriteAllText(state, "InProgress-Publish");
                    this.Publish(appName, version, workingDir, procFileP);
                    
                    File.WriteAllText(state, "Complete-Success");
                }
                catch (Exception ex)
                {
                    File.WriteAllText(state, "Complete-PublishFailed");
                    throw;
                }
             
            }
            catch (Exception e)
            {
                File.AppendAllText(logFile, e + " \r\n");
                File.AppendAllText(logFile, e.Message + " \r\n");
                File.AppendAllText(logFile, e.GetType() + " \r\n");
                File.AppendAllText(logFile, "Completed with Error... \r\n");
            }
        });//.Join();
        t.Start();

    }
    catch (Exception ex)
    {
        File.AppendAllText(logFile, ex + " \r\n");
        File.AppendAllText(logFile, ex.Message + " \r\n");
        File.AppendAllText(logFile, ex.GetType() + " \r\n");
        File.AppendAllText(logFile, "Completed with Error... \r\n");
        File.WriteAllText(state, "Complete-Error");
        //throw;
    }
}

protected override void OnLoad(EventArgs e)
{
    var buildType = Request.Form["buildType"];
    if (string.IsNullOrWhiteSpace(buildType))
    {
        this.Response.Redirect("~/");
    }

    this.isDebug = this.Request.Url.Port == 8099;

    var configFiles = new DirectoryInfo(path).EnumerateFiles("*")
                            .Where(x=> Path.GetFileNameWithoutExtension(x.Name).ToUpper() == buildType.ToUpper());

    var globalConfigFiles = new DirectoryInfo(globalPath).EnumerateFiles();

    var config = new Dictionary<string, string>();

    foreach(var configFile in configFiles)
    {
        config[configFile.Extension.Trim(".".ToCharArray())] = File.ReadAllText(configFile.FullName).Trim();
    }
    
    foreach(var configFile in globalConfigFiles)
    {
        this.globalConfig[configFile.Name] = File.ReadAllText(configFile.FullName).Trim();
    }
    

    DateTime date = DateTime.UtcNow;
    string version = date.Year + "." + date.ToString("MMdd") + "." + date.ToString("mmss") + ".0000";

    this.BuildProject(buildType, version, config);

    this.Response.Redirect("~/viewBuild.aspx?alias=" + buildType + "&version=" + version);

    base.OnLoad(e);
}
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="LeftPanel" Runat="Server">
    

</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainPanel" Runat="Server" >
    
</asp:content>