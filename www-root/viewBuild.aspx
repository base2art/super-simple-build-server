<%@ Page Language="C#" MasterPageFile="~/standard.master" Title="Home" %>
<%@ Import Namespace="System.IO" %>  

<script runat="server">

private string path = HttpContext.Current.Server.MapPath("~/app_data");
private string procFileContentC;
private string procFileContentB;
private string procFileContentP;
private string stateContent;
private string logContent;



protected string ReadAllText(string path)
{
    using(var fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
    {
        using (var sr = new StreamReader(fs))
        {
            return sr.ReadToEnd();
        }
    }
}
protected override void OnLoad(EventArgs e)
{

    base.OnLoad(e);
    
    var appName = Request.QueryString["alias"];
    var version = Request.QueryString["version"];
    
    string checkoutDirs = Path.Combine(@"c:\cicd\simple-server\checkouts", appName);
    string logDirs = Path.Combine(@"c:\cicd\simple-server\logs", appName, version);
    Directory.CreateDirectory(checkoutDirs);
    Directory.CreateDirectory(logDirs);


    String procFileC = Path.Combine(logDirs, version + ".checkout-proc");
    String procFileB = Path.Combine(logDirs, version + ".build-proc");
    String procFileP = Path.Combine(logDirs, version + ".publish-proc");
    String logFile = Path.Combine(logDirs, version + ".log");
    String state = Path.Combine(logDirs, version + ".state");
    this.procFileContentC = ReadAllText(procFileC);
    this.procFileContentB = ReadAllText(procFileB);
    this.procFileContentP = ReadAllText(procFileP);
    
    this.stateContent = ReadAllText(state);
    this.logContent = ReadAllText(logFile);
}
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="MainPanel" Runat="Server">
    
    <% if (!this.stateContent.ToUpper().Contains("COMPLETE")) { %>
        <script type = "text/JavaScript">
         <!--
            function AutoRefresh( t ) {
               setTimeout("location.reload(true);", t);
            }
            AutoRefresh(5000);
         //-->
        </script>
    <% } %>
    
    <div>
        <h3>State: <%= this.stateContent %></h3>
    </div>
    <div class="codePanelContainer">
    
        <h3>Main Log</h3>
        <div class="codePanel">
          <pre><code><%= this.logContent %></code></pre>
        </div>
    </div>
    <div class="codePanelContainer">
        <h3>Git Log</h3>
        <div  class="codePanel">
          <pre><code><%= this.procFileContentC %></code></pre>
        </div>
    </div>
    <div class="codePanelContainer">
    
        <h3>Build Log</h3>
        <div  class="codePanel">
          <pre><code><%= this.procFileContentB %></code></pre>
        </div>
    </div>
    <div class="codePanelContainer">
        <h3>Publish Log</h3>
        <div  class="codePanel">
          <pre><code><%= this.procFileContentP %></code></pre>
        </div>
    </div>
    
    

</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="LeftPanel" Runat="Server" >
    
</asp:content>