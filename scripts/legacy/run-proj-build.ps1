
$cloneUrl = "git@bitbucket.com/something.git"
$branch = "matter"
$appAlias = "project"
$verbose = $false
$testFileName = "test-deployment"

$oldDir = pwd
if (-Not (Test-Path "clean")) {
  $swallow = mkdir "clean"
}

if (-Not (Test-Path "logs")) {
  $swallow = mkdir "logs"
}

if (-Not (Test-Path "logs/$($appAlias)")) {
  $swallow = mkdir "logs/$($appAlias)"
}


$date = Get-Date
$version = "$($date.year).$($date.ToString("MMdd")).$($date.ToString("mmss")).0000"

$outputPath = "logs/$($appAlias)/$($version).log";
Set-Content -Path $outputPath -Value ""
$outputFile = Get-Item -Path $outputPath

echo "Starting..."
echo ""

cd clean

#try {

  echo "checking out..."
  echo ""
  $dest = "$($appAlias)-$($version)"
  git clone -b "$($branch)" "$($cloneUrl)" "$dest"
  cd "$($dest)"
  
  if ($verbose -eq $true) {
    echo "running in: $(pwd)"
  }
  
  Set-Content -Path ".version" -Value "$version"
  Set-Content -Path ".branch-name" -Value "$($branch)"
  #pwsh -File bob.ps1
  
  echo ""
  echo "building..."
  echo ""
  
  $output = (& pwsh -File bob.ps1 default 2>&1)
  $pwshExitCode = $LASTEXITCODE
  
  #echo $output.GetType()
  [system.io.file]::WriteAllText($outputFile.FullName, $output -join "`r`n")
  
  Echo "For build log see: $outputPath"
  if($pwshExitCode -ne 0) {
      Write-Host "BUILD FAILED"
      cd $oldDir
      exit $pwshExitCode;
  }
  
  echo ""
  echo "publishing..."
  echo ""
  
  $artifacts = Get-Content ".artifacts"
  $allFiles = Get-ChildItem -Recurse  | Resolve-Path -Relative
  
  $wc = new-object System.Net.WebClient 
  foreach($line in $artifacts) {
    if ($verbose -eq $true) {
      echo $line;
    }
    
    $pattern = "^$([System.Text.RegularExpressions.Regex]::Escape($line).Replace("\*", ".*").Replace("\?", "."))$"
    if ($verbose -eq $true) {
      echo "PATTERN $pattern"
    }
    $filteredFiles = $allFiles | Foreach-Object { $_ -replace '\\', '/' } | Select-String -Pattern $pattern
    foreach ($fileItem in $filteredFiles) {
        if ($verbose -eq $true) {
          echo "$fileItem"
        }
        
        if (-not [system.String]::IsNullOrWhitespace($testFileName)) {
          $newName = ".deploy/$testFileName.$version.zip"
          move-Item $fileItem $newName
          $fileItem = $newName
        }
        
        $fullFile = Get-Item $fileItem
        if ($verbose -eq $true) {
          echo "Uploading $($fullFile.FullName)"
        }
        
        $swallow = $wc.UploadFile("https://deployment-url.com", "$($fullFile.FullName)")
    }
  }
  
  echo ""
  echo "completed..."
  echo ""
  
#
#
#} catch { 
#
#}

cd $oldDir