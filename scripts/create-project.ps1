
$pwdRoot = Split-Path $PSScriptRoot
$pwd = Join-Path $pwdRoot "www-root/app_data"           

$projectName = Read-Host "Project Key (alpha-numeric-only)"

$projectName = $projectName -replace "[^a-zA-Z0-9-]", "-"

while ($projectName.Contains("--")) {
  $projectName = $projectName.Replace("--", "-")
}

$urlPath = Join-Path $pwd "$($projectName).git-proj"
Set-Content -Path "$urlPath" -Value ""
Write-Host "Place your git url in the file: ``$($urlPath)``"
$urlPath = Join-Path $pwd "$($projectName).branch"
Set-Content -Path "$urlPath" -Value "master"
Write-Host "Place your building branch in the file: ``$($urlPath)`` [default is master]"

