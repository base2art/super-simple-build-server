# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
 if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
  $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
  Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
  Exit
 }
}

if (-not(Test-Path "c:\WINDOWS\system32\config\systemprofile\.ssh\id_rsa.pub")) {
  Copy-Item -Recurse ~\.ssh\ c:\WINDOWS\system32\config\systemprofile\
}
icacls "c:\WINDOWS\system32\config\systemprofile\.ssh\" /grant "IIS_IUSRS:(OI)(CI)F" /T
icacls "c:\WINDOWS\system32\config\systemprofile\.ssh\" /grant "IUSR:(OI)(CI)F" /T   
