# Self-elevate the script if required
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
 if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
  $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
  Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
  Exit
 }
}


$siteName = "super-simple-build-server";
$pwdRoot = Split-Path $PSScriptRoot

$pwd = Join-Path $pwdRoot "www-root"

& 'c:\Windows\System32\inetsrv\appcmd' add apppool /name:$siteName /managedRuntimeVersion:v4.0 /managedPipelineMode:Integrated
& 'c:\Windows\System32\inetsrv\appcmd' add site /name:$siteName /physicalPath:$pwd /bindings:http/*:7099:* 
& 'c:\Windows\System32\inetsrv\appcmd' set app "$siteName/" /applicationPool:$siteName

icacls "$pwdRoot" /grant "IIS_IUSRS:(OI)(CI)F" /T
icacls "$pwdRoot" /grant "IUSR:(OI)(CI)F" /T
icacls "$pwdRoot" /grant "$($env:USERNAME):(OI)(CI)F" /T



